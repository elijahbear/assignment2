

import java.net.*;
import java.io.*;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class MyEavesdrop
 */
@WebServlet("/MyEavesdrop")
public class MyEavesdrop extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private HashMap<String,String> userMap;

    /**
     * Default constructor. 
     */
    public MyEavesdrop() {
        userMap = new HashMap<String,String>();
    }
    
    protected BufferedReader getBodyReader(URL url) throws IOException
    {
    	URLConnection urlconn = url.openConnection();
    	return new BufferedReader(new InputStreamReader(urlconn.getInputStream()));
    }
    
    protected void printBody(BufferedReader reader, PrintWriter out, String requestHistory, String type, String project) throws IOException {
    	if(requestHistory != null) {
    		out.println("Visited URLs \n");
    		out.println(requestHistory);
    	}
    	String input;
    	String output = "";
    	out.println("URL Data\n");
		while((input = reader.readLine()) != null) {
			if(input.matches("^<tr>.*[TXT].*"))
			{
				output += "http://eavesdrop.openstack.org/" + type + "/" + project + "/" + input.split("href=\\\"")[1].split("\\\">")[0] + "\n";
			}
		}
		out.println(output);
		reader.close();
    }
    
    protected URL getUrlFromParam(String type, String project, String year) throws MalformedURLException {
    	if(year != null) {
    		return new URL("http://eavesdrop.openstack.org/" + type + "/" + project + "/" + year + "/");
    	}
    	else{
    		return new URL("http://eavesdrop.openstack.org/" + type + "/" + project + "/");
    	}
    }
    
    protected void handleSession(String username, String todo)
    {
    	if(todo.matches("start")) {
    		userMap.put(username,"");
    	}
    	else {
    		userMap.remove(username);
    	}
    }
    
    protected boolean checkProject(String project) {
    	try{
	    	URL url = new URL("http://eavesdrop.openstack.org/meetings/" + project.replaceFirst("#", ""));
	    	try{
	    		url.getContent();
	    	}
	    	catch(IOException e)
	    	{
	    		return false;
	    	}
	    	return true;
    	}
    	catch(MalformedURLException me)
    	{
    		return false;
    	}
    }
    
    protected String getUrlErrors(StringBuffer url, String query, String type, String project, String year, String username, String session) {
    	String error = "";
    	if(type != null && !type.matches("irclogs") && !type.matches("meetings")) {
    		error += "Disallowed value specified for parameter type \n";
    	}
    	if(project != null && !checkProject(project)) {
    		error += "Disallowed value specified for parameter project \n";
    	}
    	if(year != null && ((2010 > Integer.parseInt(year)) || (Integer.parseInt(year) > 2015)))
    	{
    		error += "Disallowed value specified for parameter year \n";
    	}
    	if(username != null && username.matches("[ ]+"))
    	{
    		error += "Disallowed value specified for parameter username \n";
    	}
    	if(session != null && !session.matches("start") && !session.matches("end")) {
    		error += "Disallowed value specified for parameter session \n";
    	}
    	if(error != "")
    	{
    		return url.toString() + "?" + query + "\n\n" + error;
    	}
    	else
    	{
    		return error;
    	}
    }


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String errors = getUrlErrors(request.getRequestURL(), request.getQueryString(), request.getParameter("type"), request.getParameter("project"), request.getParameter("year"), request.getParameter("username"), request.getParameter("session"));
		if(errors == "")
		{
			if(request.getParameter("username") != null)
			{
				response.addCookie(new Cookie("session", request.getParameter("username")));
				handleSession(request.getParameter("username"), request.getParameter("session"));
			}
			else {
				Cookie[] cookies = request.getCookies();
				String sessionName = null;
				for(int i =0; i < cookies.length; i++)
				{
					if(cookies[i].getName().matches("session"))
					{
						sessionName = cookies[i].getValue();
					}
				}
				String requestHistory = userMap.get(sessionName);
				String type = request.getParameter("type");
				String project = request.getParameter("project");
				project = project.replaceFirst("#", "%23");
				String year = request.getParameter("year");
				URL url = getUrlFromParam(type,project,year);
				PrintWriter out = response.getWriter();
				try{
					BufferedReader in = getBodyReader(url);
					printBody(in, out, requestHistory, type, project);	
				}
				catch(IOException e)
				{
					out.println("Could not find requested resource.");
				}
				if(userMap.containsKey(sessionName))
				{
					userMap.put(sessionName, (userMap.get(sessionName) + request.getRequestURL() + "?" + request.getQueryString() + "\n\n"));
				}
			}
		}
		else
		{
			response.getWriter().println(errors);
		}
	}
	
	
}
